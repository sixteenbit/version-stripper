# Version Stripper

Remove version strings from assets.

```bash
# Change into the mu-plugins directory
cd wp-content/mu-plugins

# Download version-stripper.php
wget -N https://gitlab.com/sixteenbit/version-stripper/raw/master/version-stripper.php
```

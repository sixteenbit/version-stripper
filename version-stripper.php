<?php
/*
Plugin Name: Version Stripper
Plugin URI: https://gitlab.com/sixteenbit/version-stripper
Description: Remove version strings from assets.
Version: 1.0.0
Author: Sixteenbit
Author URI: https://sixteenbit.com
License: GPLv2 or later
Text Domain: version-stripper
*/

/**
 * Remove version strings
 *
 * @param $src
 *
 * @return mixed
 */
function rvs_remove_script_version( $src ) {
	$parts = explode( '?ver', $src );

	return $parts[0];
}

add_filter( 'script_loader_src', 'rvs_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'rvs_remove_script_version', 15, 1 );
